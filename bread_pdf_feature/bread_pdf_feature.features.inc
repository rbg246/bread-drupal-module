<?php
/**
 * @file
 * bread_pdf_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function bread_pdf_feature_node_info() {
  $items = array(
    'bread_pdf' => array(
      'name' => t('Bread pdf'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
