<?php
/**
 * @file
 * bread_pdf_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function bread_pdf_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-bread_pdf-field_converted_images'.
  $field_instances['node-bread_pdf-field_converted_images'] = array(
    'bundle' => 'bread_pdf',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_converted_images',
    'label' => 'Converted images',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'jpg',
      'file_extensions' => 'jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-bread_pdf-field_pdf_uploads'.
  $field_instances['node-bread_pdf-field_pdf_uploads'] = array(
    'bundle' => 'bread_pdf',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_pdf_uploads',
    'label' => 'PDF Uploads',
    'required' => 1,
    'settings' => array(
      'description_field' => 1,
      'file_directory' => 'pdf',
      'file_extensions' => 'pdf',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => -3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Converted images');
  t('PDF Uploads');

  return $field_instances;
}
