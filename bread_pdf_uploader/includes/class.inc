<?php


class BreadPDF {
  /**
   * defines content type
   */
  const NODE_TYPE = 'bread_pdf';
  const MODULE_NAME = 'bread_pdf_uploader';
  const FIELD_PDF_FILE = 'field_pdf_uploads';
  const FIELD_CONVERTED_IMAGES = 'field_converted_images';
  /**
   * provides path to the node pdf converter
   * @return string
   */
  static function NODE_EXECUTABLE() {
    return drupal_get_path('module', self::MODULE_NAME) . '/library/index.js';
  }
  
  /**
   * converts each page of a pdf into an image returning an array of images
   * @param $uri
   * @return array $converted_images
   */
  static function convert_pdf_to_images ($uri) {
  
    $file_path = drupal_realpath($uri);
    $converted_images = [];
    // execute node script that will stdout a json array of the image paths
    exec('node ' . self::NODE_EXECUTABLE() . ' "' . $file_path .'"', $pdf_page_image_paths);
    if (isset($pdf_page_image_paths) && is_array($pdf_page_image_paths) && sizeof($pdf_page_image_paths) > 0) {
      $pdf_page_image_paths = json_decode($pdf_page_image_paths[0]);
      $converted_images = [];
      foreach ($pdf_page_image_paths as $page_image_path) {
        watchdog('notice', 'Creating drupal managed file entry for ' . $page_image_path);
        $fn = basename($page_image_path);
        $converted_image = file_save_data(file_get_contents($page_image_path), 'private://pdf/'.basename($fn));
        if ($converted_image) {
          $converted_images[] = (array) $converted_image;
        }
      }
    }
    
    return $converted_images;
  }
  
}