var argv = process.argv;

// third cli arg is the file path and name, could make this more robust
$pdf_fn = argv[2];

var PDFImage = require("pdf-image").PDFImage;

var pdfImage = new PDFImage($pdf_fn);
pdfImage.setConvertExtension('jpg');
var numOfPages = pdfImage.numberOfPages();
var currentPage = 0;
var files = [];
// define the callback for each page creation
var image_creation_cb = function (imagePath) {
    files.push(imagePath);
    if (++currentPage < numOfPages) {
        pdfImage.convertPage(currentPage).then(image_creation_cb);
    } else {
        console.log(JSON.stringify(files));
    }
};
pdfImage.convertPage(currentPage).then(image_creation_cb);


